<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Particle Animation Preload</title>
  <style>
    /* Global Styles */
    @blue: #26bbf0;
    @green: #c2d5a0;
    @red: #cf6a6d;
    @yellow: #ffbc0d;
    @black: #343536;
    @black-2: #424950;
    @grey: #89949b;
    @grey-2: #9aa8b1;
    @light-grey: #c4c9cd;
    @light-grey-2: #dbdfe1;
    @white: #f3f4f5;

    body {
      -webkit-font-smoothing: antialiased;
      text-rendering: optimizeLegibility;
      font-family: "proxima-nova-soft", sans-serif;
      -webkit-user-select: none;
      overflow: hidden;
    }

    .vertical-centered-box {
      position: absolute;
      width: 100%;
      height: 100%;
      text-align: center;
      &:after {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -0.25em;
      }
      .content {
        box-sizing: border-box;
        display: inline-block;
        vertical-align: middle;
        text-align: left;
        font-size: 0;
      }
    }

    * {
      transition: all .3s;
    }

    /* Particle Animation Styles */
    #particles-background, #particles-foreground {
      left: -51%;
      top: -51%;
      width: 202%;
      height: 202%;
      transform: scale3d(.5, .5, 1);
    }

    /* Loader Styles */
    #loader {
      position: absolute;
      width: 100%;
      height: 100%;
      background-color: #000;
      display: flex;
      justify-content: center;
      align-items: center;
      z-index: 9999;
    }

    .loader-circle {
      width: 120px;
      height: 120px;
      border-radius: 50%;
      box-shadow: inset 0 0 0 1px rgba(255, 255, 255, .1);
      animation: fade 1.2s infinite ease-in-out;
    }

    .loader-line-mask {
      width: 60px;
      height: 120px;
      overflow: hidden;
      transform-origin: 60px 60px;
      -webkit-mask-image: -webkit-linear-gradient(top, rgba(0, 0, 0, 1), rgba(0, 0, 0, 0));
      animation: rotate 1.2s infinite linear;
    }

    .loader-line {
      width: 120px;
      height: 120px;
      border-radius: 50%;
      box-shadow: inset 0 0 0 1px rgba(255, 255, 255, .5);
    }
  </style>
</head>
<body>
  <!-- Loader -->
  <div id="loader">
    <div class="loader-circle"></div>
    <div class="loader-line-mask">
      <div class="loader-line"></div>
    </div>
  </div>

  <!-- Particle Animations -->
  <div id="particles-background" class="vertical-centered-box"></div>
  <div id="particles-foreground" class="vertical-centered-box"></div>

  <!-- Your content here -->

  <script>
    // JavaScript code to initialize the particle animations
    /*!
     * Particleground
     * @version 1.1.0
     * (The Particleground library code goes here)
     */

    // Once the page is fully loaded, hide the loader and start the animations
    window.addEventListener("load", function () {
      // Hide the loader
      var loader = document.getElementById("loader");
      loader.style.display = "none";

      // Start the particle animations
      particleground(document.getElementById('particles-foreground'), {
        dotColor: 'rgba(255, 255, 255, 1)',
        lineColor: 'rgba(255, 255, 255, 0.05)',
        minSpeedX: 0.3,
        maxSpeedX: 0.6,
        minSpeedY: 0.3,
        maxSpeedY: 0.6,
        density: 50000, // One particle every n pixels
        curvedLines: false,
        proximity: 250, // How close two dots need to be before they join
        parallaxMultiplier: 10, // Lower the number is more extreme parallax
        particleRadius: 4, // Dot size
      });

      particleground(document.getElementById('particles-background'), {
        dotColor: 'rgba(255, 255, 255, 0.5)',
        lineColor: 'rgba(255, 255, 255, 0.05)',
        minSpeedX: 0.075,
        maxSpeedX: 0.15,
        minSpeedY: 0.075,
        maxSpeedY: 0.15,
        density: 30000, // One particle every n pixels
        curvedLines: false,
        proximity: 20, // How close two dots need to be before they join
        parallaxMultiplier: 20, // Lower the number is more extreme parallax
        particleRadius: 2, // Dot size
      });
    });
  </script>
</body>
</html>
